//
//  TodoList.swift
//  SwiftUI-app
//
//  Created by Sam Maurya on 10/11/21.
//

import SwiftUI

struct TodoList: View {
    @ObservedObject var dataSource = TodoDataSource()
    
    var body: some View {
        NavigationView {
            VStack{
                if dataSource.todo.count > 0 {
                    Spacer(minLength: 0)
                    List{
                        ForEach(dataSource.todo){ todo in
                            Text(todo.text)
                        };
                    }
                    .onAppear(perform: {
                        UITableView.appearance().contentInset.top = -35
                    })
                    Text("Total TODO: \(dataSource.todo.count)")
                }else{
                    Text("Empty TODO")
                }
            }
            .navigationTitle("Todo List")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                Button("Add") {
                    //                              print("Help tapped!")
                    self.dataSource.addToDo()
                }
            }
        }
    }
}

struct TodoList_Previews: PreviewProvider {
    static var previews: some View {
        TodoList()
    }
}
