//
//  ContentView.swift
//  Shared
//
//  Created by Sam Maurya on 10/11/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView{
            List{
                NavigationLink("Shopping UI") {
                    Shopping_ui()
                }
                
                NavigationLink("Profile UI") {
                    Profile_ui()
                }
                
                NavigationLink("TodoList") {
                    TodoList()
                }
                
                NavigationLink("Dynamic List") {
                    DynamicListView()
                }
            }
            .navigationTitle("Demo Apps")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
