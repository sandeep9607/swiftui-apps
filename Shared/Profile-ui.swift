//
//  Profile-ui.swift
//  SwiftUI-app
//
//  Created by Sam Maurya on 10/11/21.
//

import SwiftUI

struct Profile_ui: View {
    var body: some View {
        GeometryReader{ geomatry in
            VStack(alignment:.leading){
                Image("nature")
                    .resizable()
                    .frame( height: geomatry.size.width)
                HStack{
                    CircleAvatarView(imageName: "nature")
                        .frame(width: 150, height: 150)
                        .offset(y:-75)
                        .padding(.bottom,-75)
                    Spacer()
                    BlueRect(numberText: "20", titleText: "Like")
                        .frame(width: 90, height: 50, alignment: .center)
                    BlueRect(numberText: "20", titleText: "Comments")
                        .frame(width: 90, height: 50, alignment: .center)
                    Spacer()
                }
                VStack(alignment:.leading,spacing:10){
                    Text("Sandeep Maurya").font(.largeTitle).fontWeight(.bold).foregroundColor(Color.black)
                    Text("Software Developer")
                        .font(.title)
                        .foregroundColor(Color.gray)
                    //                        .padding()
                }.padding()
                Spacer()
                SocialMediaButtons()
                    .padding()
                Spacer()
            }
            .edgesIgnoringSafeArea(.all)
        }
        
    }
}

struct Profile_ui_Previews: PreviewProvider {
    static var previews: some View {
        Profile_ui()
    }
}
