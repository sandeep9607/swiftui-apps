//
//  MenuSection.swift
//  SwiftUIDemo
//
//  Created by Sam Maurya on 28/10/21.
//

import Foundation
class MenuSection: Codable,Identifiable {
    let id: UUID
    let name: String
    let items: [MenuItem]
}
