//
//  BagView.swift
//  shopping-ui (iOS)
//
//  Created by Sam Maurya on 09/11/21.
//

import SwiftUI

struct BagView: View {
    
    var bagData : BagModel
    var animation : Namespace.ID
    var body: some View {
        VStack(alignment: .leading, spacing: 6, content: {
                        ZStack{
                            Color(.red)
                                .cornerRadius(15)
                            Image(bagData.image)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .padding(20)
                                .matchedGeometryEffect(id: bagData.image, in: animation)
                        }
                        Text(bagData.title)
                            .fontWeight(.heavy)
                            .foregroundColor(.gray)
            
                        Text(bagData.price)
                            .fontWeight(.heavy)
                            .foregroundColor(.black)
        })
        .padding()
        .padding(.top,10)

    }
}
