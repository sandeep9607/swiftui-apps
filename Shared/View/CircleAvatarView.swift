//
//  CircleAvatarView.swift
//  ScreenDesignSwiftUI
//
//  Created by Sam Maurya on 02/11/21.
//

import SwiftUI

struct CircleAvatarView: View {
    var imageName : String
    var body: some View {
        Image(imageName)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .clipShape(Circle())
            .shadow(radius: 10)
            .overlay(Circle().stroke(Color.white,lineWidth: 5))
    }
}

struct CircleAvatarView_Previews: PreviewProvider {
    static var previews: some View {
        CircleAvatarView(imageName: "user")
    }
}
