//
//  SocialMediaButtons.swift
//  ScreenDesignSwiftUI
//
//  Created by Sam Maurya on 02/11/21.
//

import SwiftUI

struct SocialMediaButtons: View {
    var body: some View {
        HStack(spacing:20){
            Image("facebook")
                .resizable()
                .frame(width: 60, height: 60, alignment: .center)
                .cornerRadius(10)
            Image("instagram")
                .resizable()
                .frame(width: 60, height: 60, alignment: .center)
                .cornerRadius(10)
            Image("linkedin")
                .resizable()
                .frame(width: 60, height: 60, alignment: .center)
                .cornerRadius(10)
            Image("tw")
                .resizable()
                .frame(width: 60, height: 60, alignment: .center)
                .cornerRadius(10)
        }
        
    }
}

struct SocialMediaButtons_Previews: PreviewProvider {
    static var previews: some View {
        SocialMediaButtons()
    }
}
