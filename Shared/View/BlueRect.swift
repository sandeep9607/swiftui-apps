//
//  BlueRect.swift
//  ScreenDesignSwiftUI
//
//  Created by Sam Maurya on 02/11/21.
//

import SwiftUI

struct BlueRect: View {
    var numberText: String
    var titleText: String
    
    var body: some View {
        ZStack{
            Color.blue
            VStack{
                Text(numberText)
                    .foregroundColor(Color.white)
                    .fontWeight(.bold)
                Text(titleText)
                    .font(.caption)
                    .foregroundColor(Color.white)
                    .fontWeight(.medium)
            }
        }
    }
}

struct BlueRect_Previews: PreviewProvider {
    static var previews: some View {
        BlueRect(numberText: "", titleText: "")
    }
}
