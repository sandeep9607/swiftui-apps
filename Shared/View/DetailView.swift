//
//  DetailView.swift
//  shopping-ui (iOS)
//
//  Created by Sam Maurya on 09/11/21.
//

import SwiftUI

struct DetailView: View {
    
    @Binding var bagData : BagModel!
    @Binding var show : Bool
    var animation :Namespace.ID
    //Initialization ...
    @State var selectedColor = Color.red
    
    var body: some View {
        VStack{
            HStack{
                VStack(alignment:.leading,spacing:5){
                    Button(action: {
                        withAnimation(.easeOut){show.toggle()}
                    }, label: {
                        Image(systemName: "chevron.left")
                            .font(.title)
                            .foregroundColor(.white)
                    })
                    
                    Text("Aristocratic Hand Bag")
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .padding(.top)
                    
                    Text(bagData.title)
                        .font(.largeTitle)
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                }
                
                Spacer(minLength: 0)
                
                Button(action: {}, label: {
                    Image(systemName: "cart")
                        .font(.title)
                        .foregroundColor(.white)
                })
            }
            .padding()
            .padding(.top,UIApplication.shared.windows.first?.safeAreaInsets.top)
            
            HStack(spacing:10){
                VStack(alignment: .leading, spacing: 6, content: {
                    Text("Price")
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                    
                    Text(bagData.price)
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                })
                Spacer()
                Image(bagData.image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                //Hero animation
                    .matchedGeometryEffect(id: bagData.image, in: animation)
            }
            .padding()
            .padding(.top,10)
            
            
            
            VStack{
                HStack{
                    VStack(alignment: .leading, spacing: 8){
                        Text("Color")
                            .foregroundColor(.gray)
                        HStack( spacing: 15){
                            ColorButton(color: Color.red, selectedColor: $selectedColor)
                            ColorButton(color: Color.green, selectedColor: $selectedColor)
                            ColorButton(color: Color.yellow, selectedColor: $selectedColor)
                        }
                    }
                    Spacer(minLength: 0)
                    
                    VStack(alignment: .leading, spacing: 8, content: {
                        Text("Size")
                            .fontWeight(.semibold)
                            .foregroundColor(.black)
                        
                        Text("12 cm")
                            .fontWeight(.bold)
                            .foregroundColor(.black)
                    })
                }
                .padding(.horizontal)
                Spacer()
            }
            .background(Color.white)
//            Spacer(minLength: 0)
            
        }
        .background(Color.blue.ignoresSafeArea(.all,edges: .top))
        .background(Color.white.ignoresSafeArea(.all, edges: .bottom))
        .onAppear{
            selectedColor = Color.red
        }
    }
}

//struct DetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailView()
//    }
//}
