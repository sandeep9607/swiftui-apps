//
//  TabButton.swift
//  shopping-ui (iOS)
//
//  Created by Sam Maurya on 08/11/21.
//

import SwiftUI

struct TabButton: View {
    var title : String
    @Binding var selectedTab : String
    var animation : Namespace.ID
    
    var body: some View {
        Button(action: {
            withAnimation(.spring()){selectedTab = title}
        }, label: {
            VStack(alignment:.leading){
                Text(title)
                    .fontWeight(.heavy)
                    .foregroundColor(selectedTab == title ? .black : .gray)
                if selectedTab == title {
                    Capsule()
                        .fill(Color.black)
                        .frame(width: 40, height: 4, alignment: .center)
                        .matchedGeometryEffect(id: "Tab", in: animation)
                }
      
            }
        })
        // default width
        .frame(width: 100)
    }
}
