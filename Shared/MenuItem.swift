//
//  MenuItem.swift
//  SwiftUIDemo
//
//  Created by Sam Maurya on 28/10/21.
//

import Foundation
class MenuItem: Codable,Identifiable {
    let id: UUID
    let name:String
    let photoCredit: String
    let price: Double
    let description: String
    let restrictions: [String]
    
    init(id:UUID,name:String,photoCredit:String,price:Double,description: String,restrictions:[String]) {
        self.id = id
        self.name = name
        self.photoCredit = photoCredit
        self.price = price
        self.description = description
        self.restrictions = restrictions
    }
}
