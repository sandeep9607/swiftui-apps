//
//  SwiftUI_appApp.swift
//  Shared
//
//  Created by Sam Maurya on 10/11/21.
//

import SwiftUI

@main
struct SwiftUI_appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
//            AsyncImage_ui()
        }
    }
}
