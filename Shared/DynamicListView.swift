//
//  DynamicListView.swift
//  SwiftUIDemo
//
//  Created by Sam Maurya on 28/10/21.
//

import SwiftUI

struct DynamicListView: View {
    @State var sections = [MenuSection]()
    var body: some View {
        
        NavigationView{
            List{
                ForEach(sections){section in
                    Section(header: Text("\(section.name)")) {
                        ForEach(section.items){item in
                            NavigationLink(
                                destination:  ItemDetailView(item: item))
                                 {
                                    VStack(alignment: .leading, spacing: 10){
                                        HStack{
                                            Text("\(item.name)")
                                                .font(.headline)
                                            Spacer()
                                            Text(String(format: "%.2f", item.price))
                                        }
                                        Text("\(item.description)")
                                    }
                                }
                        }
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitle("Menu")
            .onAppear(perform: loadData)
        }
    }
    
    func loadData() {
        if let url = Bundle.main.url(forResource: "item", withExtension: "json") {
                do {
                    let data = try Data(contentsOf: url)
                    let decoder = JSONDecoder()
                    let jsonData = try decoder.decode([MenuSection].self, from: data)
                    DispatchQueue.main.async {
                        sections = jsonData
                    }
                } catch {
                    print("error:\(error)")
                }
            }
    }
}

struct DynamicListView_Previews: PreviewProvider {
    static var previews: some View {
        DynamicListView()
    }
}
