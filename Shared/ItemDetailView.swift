//
//  ItemDetailView.swift
//  SwiftUIDemo
//
//  Created by Sam Maurya on 01/11/21.
//

import SwiftUI

struct ItemDetailView: View {
    let item : MenuItem
    var body: some View {
        VStack(alignment: .leading, spacing: 10){
            HStack{
                Text("\(item.name)")
                    .font(.headline)
                Spacer()
                Text(String(format: "$%.2f", item.price))
            }
            Text("Photo Credit: \(item.photoCredit)")
            Text("\(item.description)")
            Spacer()
        }.padding()
        .navigationBarTitle(Text(item.name), displayMode: .inline )
    }
}

struct ItemDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ItemDetailView(item: MenuItem(id: UUID(), name: "dfg", photoCredit: "asf", price: 50.0, description: "dfg", restrictions: ["asdg","asdf"]))
    }
}
