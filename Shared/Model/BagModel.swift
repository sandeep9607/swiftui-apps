//
//  BagModel.swift
//  shopping-ui (iOS)
//
//  Created by Sam Maurya on 08/11/21.
//

import Foundation

struct BagModel:Identifiable {
    var id = UUID().uuidString
    var image : String
    var title: String
    var price: String
}

var bags = [
    BagModel(image: "black-bag", title: "Black Bag", price: "$123"),
    BagModel(image: "brown-bag", title: "Brown Bag", price: "$123"),
    BagModel(image: "gray-bag", title: "Red Bag", price: "$123"),
    BagModel(image: "red-bag", title: "pink Bag", price: "$123"),
    BagModel(image: "brown-bag", title: "brown Bag", price: "$123"),
    BagModel(image: "gray-bag", title: "gray Bag", price: "$123"),
]

var scrollTabs = ["Hand Bag","Jwellery","Footwear","Dresses","Beauty"]
