//
//  DataSource.swift
//  todo-swiftui
//
//  Created by Sam Maurya on 03/11/21.
//

import Foundation
struct ToDo: Identifiable {
    let id: UUID
    let text: String
}

class TodoDataSource: ObservableObject {
    @Published var todo: [ToDo] = []
    var counter = 0
    init() {
            addToDo()
    }
    func addToDo()  {
        counter += 1
        todo.append(ToDo(id: UUID(), text: "buy milk \(counter)"))
    }
 }
